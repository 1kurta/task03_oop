package com.epam1;

public class Helicopter extends Aircraft {

    private String hover;

    @Override
    public String buzz() {
        return "yyy";
    }

    @Override
    public String toString() {
        return "Helicopter{" + super.toString()+
                "hover='" + hover + '\'' +
                '}';
    }

    public Helicopter(String takeoff, String fly, String land, String hover) {
        super(takeoff, fly, land);
        this.hover = hover;


    }
}
