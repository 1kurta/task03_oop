package com.epam1;

public class Aplication {

    public static void main(String[] args) {
        Aircraft myHelicopter = new Helicopter("Vertical","Smoothly","Hard","Jump");
        System.out.println(myHelicopter);

        Aircraft  myAirplane = new Airplane("Horizontal","Quickly","Forced","Degrees");
        System.out.println(myAirplane);

        Aircraft myBlimp =  new Blimp("Vertical","Smoothly","Water landing","Dered");
        System.out.println(myBlimp);

        print(myHelicopter);
        print(myAirplane);
        print(myBlimp);

    }

    public  static  void print(Aircraft aircraft) {
        System.out.println(aircraft);
        System.out.println(aircraft.buzz());
    }
}
