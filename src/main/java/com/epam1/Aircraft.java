package com.epam1;

public abstract class  Aircraft {
    private String takeoff;
    private String fly;
    private String land;

    public Aircraft(String takeoff,String  fly,String land) {
        this.takeoff = takeoff;
        this.fly = fly;
        this.land = land;
    }

    public abstract String buzz();

    public String getTakeoff() {
        return takeoff;
    }

    public void setTakeoff(String takeoff) {
        this.takeoff = takeoff;
    }

    public String getFly() {
        return fly;
    }

    public void setFly(String fly) {
        this.fly = fly;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "takeoff='" + takeoff + '\'' +
                ", fly='" + fly + '\'' +
                ", land='" + land + '\'' +
                '}';
    }
}
