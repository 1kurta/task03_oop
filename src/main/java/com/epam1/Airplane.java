package com.epam1;

public class Airplane extends Aircraft {
     private String bank;

    public Airplane(String takeoff, String fly, String land, String bank) {
        super(takeoff, fly, land);
        this.bank = bank;
    }

    @Override
    public String buzz() {
        return "gugugu";
    }

    @Override
    public String toString() {
        return "Airplane{" + super.toString() +
                "bank=" + bank +
                '}';
    }


}
