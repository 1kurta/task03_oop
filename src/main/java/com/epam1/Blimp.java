package com.epam1;

public class  Blimp extends Aircraft {
     private String deflate;


    public Blimp(String takeoff, String fly, String land, String deflate) {
        super(takeoff, fly, land);
        this.deflate = deflate;
    }

    @Override
    public String buzz() {
        return "rrrr";
    }

    @Override
    public String toString() {
        return "Blimp{" + super.toString()+
                "deflate=" + deflate +
                '}';
    }
}
